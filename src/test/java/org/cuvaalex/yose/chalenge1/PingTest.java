package org.cuvaalex.yose.chalenge1;

import org.fest.assertions.Assertions;
import org.junit.Test;


/**
 * Created by admin on 24.12.13.
 */

public class PingTest {


    @Test
    public void shouldReturnAliveWhenPing() {
        Ping ping = new Ping();
        boolean response = ping.alive();

        Assertions.assertThat(response).isTrue();
    }
}
