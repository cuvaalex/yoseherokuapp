package org.cuvaalex.yose.chalenge1;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by admin on 24.12.13.
 */
@Path("/")
public class Ping implements IPing {
    @GET
    @Path("/ping")
    @Produces("application/json")
    public boolean alive() {
        return true;
    }
}
